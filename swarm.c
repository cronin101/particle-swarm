#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <limits.h>
/* Header files required for Xlib and OpenGL rendering*/
#include <X11/X.h>
#include <X11/Xlib.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>
/* For display initialisation */
Display *dpy;
Window root;
GLint att[] = {GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None};
XVisualInfo *vi;
Colormap cmap;
XSetWindowAttributes swa;
Window win;
GLXContext glc;
XWindowAttributes gwa;
XEvent xev;

const XLOW = -1000, XHIGH = 1000, YLOW = -1000, YHIGH = 1000;
/* Number of particles in swarm */
int NUMPARTICLES = 500;

static int lowestx,lowesty,highestx,highesty,sidestep;
static float theomega,alpha1,alpha2;

typedef struct
{
/* r\ = position, o\ = history, v\ = velocity, n\ = nostalgia, f\ = fitness */
  double rx,ry,ox,oy,vx,vy,n,nx,ny,f,p;
}Particle_t;

typedef struct
{
  double dx,dy;
}social_effect;

double socialamount;
/* Particle_p is a pointer to a Particle_t object */
typedef Particle_t *Particle_p;

typedef int row[2001];
  
static row * pixel;

int allconverged( Particle_p p ){
  int result = 1;
  int non = 0;
  int i;
  for ( i = 0; i<=NUMPARTICLES-1; i++){
    if (p[i].f >= 0.00000001){
      result = 0;
      non++;
    }
  }
  //printf("%d unconverged.\n",non);
  return result;
}

int alldiverged( Particle_p p ){
  int result = 1;
  int i;
  for ( i = 0; i<=NUMPARTICLES-1; i++){
    if (p[i].rx <= 2000 && p[i].rx >= -2000 && p[i].ry <= 2000 && p[i].ry >= -2000 ){
      result = 0;
      break;
    }
  }
  //printf("%d unconverged.\n",non);
  return result;
}

/* All particles are initialised with random values within the search space */
void initparticles( Particle_p p ){
  int i;
  for ( i = 0; i <= NUMPARTICLES-1; i++ ){
    p[i].rx = ( (double) rand() / (double)RAND_MAX * (float) 2*XHIGH - (float) XHIGH);
    p[i].ry = ( (double) rand() / (double)RAND_MAX * (float) 2*XHIGH - (float) XHIGH);
    p[i].vx = ( (double) rand() / (double)RAND_MAX * (float) XHIGH - (float) XHIGH/2);
    p[i].vy = ( (double) rand() / (double)RAND_MAX * (float) XHIGH - (float) XHIGH/2);
    p[i].nx = p[i].rx;
    p[i].ny = p[i].ry;
    p[i].n = INT_MAX;
    p[i].f = INT_MAX;
    p[i].ox = p[i].rx;
    p[i].oy = p[i].ry;
    p[i].p = 0.01*(XHIGH - XLOW);
  }
}

void findboundaries( Particle_p p ){
  int i;
  lowestx = 0;
  lowesty = 0;
  highestx = 0;
  highesty = 0;
  for ( i = 0; i <= NUMPARTICLES-1; i++){
    lowestx = lowestx > p[i].rx? p[i].rx : lowestx;
    lowesty = lowesty > p[i].ry? p[i].ry : lowesty;
    highestx = highestx < p[i].rx? p[i].rx : highestx;
    highesty = highesty < p[i].ry? p[i].ry : highesty;
  }
  lowestx = lowestx > -100? -100 : lowestx;
  lowesty = lowesty > -100? -100 : lowesty;
  highestx = highestx < 100? 100 : highestx;
  highesty = highesty < 100? 100 : highesty;

  lowestx = lowestx < lowesty? lowesty : lowestx;
  lowesty = lowesty < lowestx? lowestx :lowesty;
  highestx = highestx < highesty? highesty : highestx;
  highesty = highesty < highestx? highestx : highesty;

  lowestx = lowestx < -highestx? lowestx : -highestx;
  highestx = highestx < -lowestx? -lowestx : highestx;
  lowesty = lowesty < -highesty? lowesty : -highesty;
  highesty = highesty < -lowesty? -lowesty : highesty;

  lowestx = lowestx < -1000? -1000 : lowestx;
  lowesty = lowesty < -1000? -1000 : lowesty;
  highestx = highestx > 1000? 1000 : highestx;
  highesty = highesty > 1000? 1000 : highesty;
}

/* Particles are translated by their velocity */
void moveparticles( Particle_p p ){
  int i;
  for ( i = 0; i <= NUMPARTICLES-1; i++ ){
    p[i].ox = p[i].rx;
    p[i].oy = p[i].ry;
    p[i].rx += p[i].vx;
    p[i].ry += p[i].vy;
  }
}

void pixelcount( Particle_p p ){
  int i;
  int j;
  int x,y;
  for (i = 0; i <= 2000; i++){
    for (j = 0; j <= 2000; j++){
      pixel[i][j] = 0;
    }
  }
  for (i = 0; i <= NUMPARTICLES -1; i++){
    x = rint(p[i].rx) + 1000;
    y = rint(p[i].ry) + 1000;
    if ( x >= 0 && x<= 2000 && y >= 0 && y <= 2000){
      pixel[x][y]++;
    }
  }
}

social_effect considerneighbours( double rx, double ry){
 /* Find a pixel in 25*25 grid and move to it if it is more popular */
  int x,y,xtarget,ytarget;
  double bestx,besty;
  social_effect result;
  bestx = rx;
  besty = ry;
  x= rint(rx);
  y= rint(ry);
  xtarget = (int)(((float)rand() / RAND_MAX) * 48) - 24;
  ytarget = (int)(((float)rand() / RAND_MAX) * 48) - 24;

/* Return vector pointing to it */
  result.dx = xtarget-1000 >= 0 && xtarget -1000 <= 2000 && pixel[xtarget-1000][ytarget-1000] > pixel[x-1000][y-1000]? xtarget - x: x;
  result.dy = ytarget-1000 >= 0 && ytarget -1000 <= 2000 && pixel[xtarget-1000][ytarget-1000] > pixel[x-1000][y-1000]? ytarget - y: y;
  return result;
}

/* Plots points gathered from particle array p into scene*/
void drawparticles( Particle_p p){
  int i,j,x,y;
  float weight = 0;
  glClearColor(0.,0.,0.,0.);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  findboundaries(p);
  //printf("Showing X: %d..%d, Y: %d..%d.\n",lowestx,highestx,lowesty,highesty);
  glOrtho(lowestx,highestx,lowesty,highesty,1.,20.);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  gluLookAt(0.,0.,10.,0.,0.,0.,0.,1.,0.);
  
  glColor3f(0.2,0.2,0.2);
  /* Axes plotted */
  glBegin(GL_LINE);
  for (i = 0; i<= 10; i++){
    glVertex3f(100*i,YHIGH,-1.);
    glVertex3f(100*i,YLOW,-1.);
    glVertex3f(XHIGH, 100*i, -1.);
    glVertex3f(XLOW, 100*i, -1.);
    glVertex3f(-100*i,YHIGH,-1.);
    glVertex3f(-100*i,YLOW,-1.);
    glVertex3f(XHIGH, -100*i, -1.);
    glVertex3f(XLOW, -100*i, -1.);
  }
  glEnd();
   glColor3f(0.1,0.1,0.1);
  /* Minor axes plotted */
  glBegin(GL_LINE);
  for (i = 0; i<= 40; i++){
    glVertex3f(25*i,YHIGH,-2.);
    glVertex3f(25*i,YLOW,-2.);
    glVertex3f(XHIGH, 25*i, -2.);
    glVertex3f(XLOW, 25*i, -2.);
    glVertex3f(-25*i,YHIGH,-2.);
    glVertex3f(-25*i,YLOW,-2.);
    glVertex3f(XHIGH, -25*i, -2.);
    glVertex3f(XLOW, -25*i, -2.);
  }
  glEnd();
/* Each point in the array is positioned */
/*  for( i = 0; i <= NUMPARTICLES-1; i++ ){
    glColor3f(0.3,0.,0.); 
    if(p[i].f <= 1000){
      glColor3f(0.,0.3,0.);
    }
    if(p[i].f > 100000){
      glColor3f(0.,0.,0.3);
    }
    glBegin(GL_LINE);
    glVertex3f(p[i].ox,p[i].oy,0.);
    glVertex3f(p[i].rx,p[i].ry,0.);
    glEnd();
  }*/
  pixelcount(p);
  for( i = 0; i <= NUMPARTICLES-1; i++ ){
    x =(int) p[i].rx + 1000;
    y =(int) p[i].ry + 1000;
    if ( x >= 0 && x<= 2000 && y >= 0 && y <= 2000){
      weight = (float)pixel[x][y];
    }
    glColor3f(weight/5,weight/2,weight/10);
    glBegin(GL_POINTS);
    glVertex3f(p[i].rx+.5,p[i].ry+.5,1.);
    glEnd();
  }
}
/* Completed buffer is sent to the screen */
void renderscene( Particle_p p ){
  XGetWindowAttributes(dpy,win,&gwa);
  glViewport(0,0,gwa.width,gwa.height);
  drawparticles(p);
  glXSwapBuffers(dpy,win);
}
/* Display is initialised and a new window is opened */
void initdisplay( Particle_p p){
  dpy = XOpenDisplay(NULL);
  if (dpy == NULL){
    printf("\n\tcannot connect to X server\n\n");
    exit(0);
  }
  root = DefaultRootWindow(dpy);

  vi = glXChooseVisual(dpy,0,att);
  if(vi == NULL){
    printf("\n\tno appropriate visual found\n\n");
    exit(0);
  }

  cmap = XCreateColormap(dpy,root,vi->visual,AllocNone);
  swa.colormap = cmap;
  swa.event_mask = ExposureMask | KeyPressMask;

  win = XCreateWindow(dpy,root,0,0,600,600,0,vi->depth,InputOutput,vi->visual,CWColormap | CWEventMask, &swa);

  XMapWindow(dpy,win);
  XStoreName(dpy,win, "Particle Swarm Optimisation");

  glc = glXCreateContext(dpy,vi,NULL,GL_TRUE);
  glXMakeCurrent(dpy,win,glc);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POINT_SMOOTH_HINT, GL_DONT_CARE);

  renderscene( p );
}

/* Waits until a keypress event, then exits */
void holduntilkeypress(){
  while(1){
    XNextEvent(dpy, &xev);
    if(xev.type == KeyPress){
      glXMakeCurrent(dpy,None,NULL);
      glXDestroyContext(dpy,glc);
      XDestroyWindow(dpy,win);
      XCloseDisplay(dpy);
      exit(0);
    }
  }
}

/* Updates current fitness of all particles, 
 * also updates nostalgia if there is a new personal best.
 * Returns the index of the particle with the best nostalgia
 * as this nostalgia is the 'global best'.*/
int testfitness(Particle_p p){
  int i,g;
  g=0;
  for (i = 0; i <= NUMPARTICLES-1; i++){
    p[i].f=pow(p[i].rx,2) + pow(p[i].ry,2);
    if(p[i].f < p[i].n){
      p[i].n = p[i].f;
      p[i].nx = p[i].rx;
      p[i].ny = p[i].ry;
    }
    if(p[g].n > p[i].n){
      g = i;
    }
  }
  return g;
}
/* Updates each particle's velocity according to parameters 
 * that govern how much it is influenced by past velocity, 
 * nostalgia and the global minima. */
void updatevelocities(int g, Particle_p p){
  int i,j;
  double gx,gy,omega,a1,a2,r1,r2,l;
  social_effect peer_pressure;
  /* Omega is the influence of past velocity */
  omega = theomega;
  /* a1 is the influence of nostalgia */
  a1 = alpha1;
  /* a2 is the influence of the global minima. */
  a2 = alpha2;
  gx = p[g].nx;
  gy = p[g].ny;
  //printf("Best is (%f,%f) with %f.\n",p[g].nx,p[g].ny,p[g].n); 
  for (i = 0; i <= NUMPARTICLES-1; i++){
    r1 = (float)rand()/RAND_MAX;
    r2 = (float)rand()/RAND_MAX;
    p[i].vx = ((omega*p[i].vx) + ((a1*r1)*(p[i].nx - p[i].rx)) + ((a2*r2)*(gx - p[i].rx)));
    p[i].vy = ((omega*p[i].vy) + ((a1*r1)*(p[i].ny - p[i].ry)) + ((a2*r2)*(gy - p[i].ry)));
    
    if(sidestep == 1){
      /* Sidesteps performed
       * to minimise axis bias */
      l = 0.1;
      /* X Dimension */
      if (fabs(p[i].rx - p[i].nx) < p[i].p && fabs(p[i].rx - gx) < p[i].p && p[i].vx < p[i].p){
        p[i].vx = p[i].vx <0? p[i].vx + l*p[i].p : p[i].vx - l*p[i].p;
        p[i].p = p[i].p/4;
      }
      /* Y Dimension */
      if (fabs(p[i].ry - p[i].ny) < p[i].p && fabs(p[i].ry - gy) < p[i].p && p[i].vy < p[i].p){
        p[i].vy = p[i].vy < 0? p[i].vy + l*p[i].p : p[i].vy - l*p[i].p;
        p[i].p = p[i].p/4; 
      }
    }
    for (j = 0; j >= 10; j++){
      /* Pixels might move towards a more popular pixel in 25x25 pixel grid */
      peer_pressure = considerneighbours(p[i].rx,p[i].ry);
      p[i].vx += peer_pressure.dx/10;
      p[i].vy += peer_pressure.dy/10;
    }
    /* Clip velocities to maximum velocity */
    p[i].vx = p[i].vx < -(XHIGH-XLOW)/5? -(XHIGH-XLOW)/5 : p[i].vx;
    p[i].vx = p[i].vx > (XHIGH-XLOW)/5? (XHIGH-XLOW)/5 : p[i].vx;
    p[i].vy = p[i].vy < -(XHIGH-XLOW)/5? -(XHIGH-XLOW)/5 : p[i].vy;
    p[i].vy = p[i].vy > (XHIGH-XLOW)/5? (XHIGH-XLOW)/5 : p[i].vy;
  }
}
/* Main execution thread, follows the PSO algorithm and calls
 * appropriate functions to display the results */
int main(int argc, char *argv[]){
int i;
int earlybreak = 0;
  NUMPARTICLES =atoi(argv[1]);
  theomega = atof(argv[2]);
  alpha1 = atof(argv[3]);
  alpha2 = atof(argv[4]);
  sidestep = atoi(argv[6]);
  pixel = malloc(2001*sizeof(row));
  srand(time(0));
  Particle_p p = malloc(NUMPARTICLES*sizeof(Particle_t));
  initparticles( p );
  if(atoi(argv[5]) != 0){
      initdisplay( p );
  }
  while(i++<10000){
    if (alldiverged(p) == 1){
      printf("D\n");
      earlybreak = 1;
      break;
    }else if (allconverged(p) == 1){
      printf("%d\n",i);
      earlybreak = 1;
      break;
    }
    //printf("iteration %d\n",i);
    updatevelocities(testfitness(p),p);
    moveparticles(p);
    if(atoi(argv[5]) != 0){
      renderscene(p);
    }
  }
  if ( earlybreak == 0){
    printf("O\n");
  }
  //holduntilkeypress();
}
